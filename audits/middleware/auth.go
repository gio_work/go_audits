package middleware

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	. "audits/config"
)


var config = Config{}

func Auth(next http.Handler) http.Handler {
	config.Read()

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			panic(err)
		}

		var values map[string]interface{}
		json.Unmarshal(data, &values)

		if value, found := values["apiKey"]; found {
			if config.API_KEY != value {
				http.Error(w, http.StatusText(401), 401)
      			return
			}
		} else {
			http.Error(w, http.StatusText(403), 403)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(data))

		next.ServeHTTP(w, r)
	})
}