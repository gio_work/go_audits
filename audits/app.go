package main

import (
	"log"
	"net/http"
	"encoding/json"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"

	. "audits/config"
	. "audits/service"
	. "audits/models"
	. "audits/middleware"
)

var config = Config{}
var database = Database{}

func Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.Logger,
		middleware.DefaultCompress,
		middleware.RedirectSlashes,
		middleware.Recoverer,
		Auth,
	)

	router.Post("/api/auditlog", CreateAuditLog)

	return router
}


// POST a new auditlog
func CreateAuditLog(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var auditlog AuditLog

	if err := json.NewDecoder(r.Body).Decode(&auditlog); err != nil {
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
		return
	}

	if err := database.Insert(auditlog); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	render.JSON(w, r, auditlog)
}

func init() {
	config.Read()

	database.Server = config.MongoDB.ConnectionString
	database.Database = config.MongoDB.Database
	database.Collection = config.MongoDB.TargetCollection
	database.Connect()
}

func main() {
	router := Routes()

	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("%s %s\n", method, route) // Walk and print out all routes
		return nil
	}

	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("Logging err: %s\n", err.Error()) // panic if there is an error
	}

	log.Println("Serving application at PORT :" + config.PORT)
	log.Fatal(http.ListenAndServe(":"+config.PORT, router))
}