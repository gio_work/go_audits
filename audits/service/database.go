package service

import (
	"log"
	. "audits/models"
	mgo "gopkg.in/mgo.v2"
)

type Database struct {
	Server 	   string
	Database   string
	Collection string
}

var db *mgo.Database

// Establish a connection to database
func (m *Database) Connect() {
	session, err := mgo.Dial(m.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(m.Database)
}

// Insert a auditlog into database
func (m *Database) Insert(auditlog AuditLog) error {
	err := db.C(m.Collection).Insert(&auditlog)
	return err
}