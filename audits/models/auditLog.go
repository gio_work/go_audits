package models

type AuditLog struct {
	Name           string        `json:"name"`
	CompanyId      string        `json:"companyId"`
	SourceSystem   string        `json:"sourceSystem"`
	AuditDirection string        `json:"auditDirection"`
}
