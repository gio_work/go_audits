package config

import (
	"log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

// Represents database server and credentials
type Config struct {
	PORT    string `yaml:"listenPort"`
	MongoDB struct {
		ConnectionString string `yaml:"connectionString"`
		Database         string `yaml:"database"`
		TargetCollection string `yaml:"targetCollection"`
	} `yaml:"mongoDb"`
	API_KEY string `yaml:"apiKey"`
}

// Read and parse the config file
func (c *Config) Read() {
	yamlFile, err := ioutil.ReadFile("config.yaml")
    if err != nil {
        log.Printf("yamlFile.Get err   #%v ", err)
	}
	
	err = yaml.Unmarshal(yamlFile, c)
	
    if err != nil {
        log.Fatalf("Unmarshal: %v", err)
	}
}
